// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "SomeCube.generated.h"

/**
 * 
 */
UCLASS()
class KUFLEXTESTTASK2_API ASomeCube : public AStaticMeshActor
{
    GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

private:
    FTimerHandle m_timerHandle;
    UMaterialInstanceDynamic* m_material;

    void ChangeColor();
};
