// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "GameFramework/Actor.h"
#include "SomeParticles.generated.h"

UCLASS()
class KUFLEXTESTTASK2_API ASomeParticles : public AActor
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

private:
    FTimerHandle m_timerHandle;
    UMaterialInstanceDynamic* m_material;

    void ChangeColor();

};
