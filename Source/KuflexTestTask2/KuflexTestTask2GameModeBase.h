// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KuflexTestTask2GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class KUFLEXTESTTASK2_API AKuflexTestTask2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
