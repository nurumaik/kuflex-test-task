// Fill out your copyright notice in the Description page of Project Settings.


#include "SomeCube.h"
#include "TimerManager.h"
#include "Components\StaticMeshComponent.h"
#include "Math\UnrealMathUtility.h"

void ASomeCube::BeginPlay() {
    Super::BeginPlay();

    m_material = UMaterialInstanceDynamic::Create(GetStaticMeshComponent()->GetMaterial(0), this);
    GetStaticMeshComponent()->SetMaterial(0, m_material);
    GetWorldTimerManager().SetTimer(m_timerHandle, this, &ASomeCube::ChangeColor, 2.f, true, 2.f);
}

void ASomeCube::ChangeColor() {
    m_material->SetVectorParameterValue(TEXT("Color"), FColor(FMath::RandRange(0, 255), FMath::RandRange(0, 255), FMath::RandRange(0, 255)));
}

