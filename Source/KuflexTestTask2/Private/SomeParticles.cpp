// Fill out your copyright notice in the Description page of Project Settings.


#include "SomeParticles.h"
#include "TimerManager.h"
#include "Materials/MaterialInterface.h"
#include "Particles\ParticleSystemComponent.h"
#include "Math\UnrealMathUtility.h"

void ASomeParticles::BeginPlay() {
    Super::BeginPlay();

    TInlineComponentArray<UParticleSystemComponent*, 1> particleSystems;
    GetComponents(particleSystems);
    auto particleSystem = particleSystems[0];
    m_material = particleSystem->CreateNamedDynamicMaterialInstance(TEXT("MainMaterial"), particleSystem->GetMaterial(0));
    GetWorldTimerManager().SetTimer(m_timerHandle, this, &ASomeParticles::ChangeColor, 2.f, true, 2.f);
}

void ASomeParticles::ChangeColor() {
    m_material->SetVectorParameterValue(TEXT("Color"), FColor(FMath::RandRange(0, 255), FMath::RandRange(0, 255), FMath::RandRange(0, 255)));
}

